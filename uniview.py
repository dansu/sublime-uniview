import sublime, sublime_plugin
import socket
#import User.SendKeysCtypes as SendKeys

def _send_msg(addr, msg):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect(addr)
        try:
            s.sendall(msg.encode())
        except Exception as e:
            print(e)

    except socket.timeout:
        print( 'Connection to {0}:{1} timed out'.format(host, str(port)) )
    except socket.error as e:
        print( 'Connection error ({0} {1})'.format(e.errno, e.strerror) )
        print( 'Could not connect to {0}:{1}'.format(addr[0], str(addr[1])) )
    finally:
        s.close()

class Uniview(object):
    def __init__(self):
        self.settings_file = 'uniview.sublime-settings'
        self.settings = sublime.load_settings(self.settings_file)
        self.addr = (self.settings.get('ip', '127.0.0.1'), self.settings.get('port', 22000))
    
    def send(self, cmd):
        try:
            sublime.set_timeout_async(lambda: _send_msg(self.addr, '{0}\n'.format(cmd)), 10)
        except AttributeError:
            sublime.set_timeout(lambda: _send_msg(self.addr, '{0}\n'.format(cmd)), 10)
        
class UvCommandCommand(sublime_plugin.TextCommand):
    def run(self, edit, cmd=None):
        if cmd != None:
            u = Uniview()
            u.send(cmd)
        else:
            sublime.active_window().show_input_panel("text event:", "", self.on_done, None, None)

    def on_done(self, text):
        self.view.run_command("uv_command",{"cmd": text})

class UvLauncherCommand(sublime_plugin.TextCommand):

    def run(self, edit, cmd=None):
        settings = sublime.load_settings('uniview.sublime-settings')
        usettings = sublime.load_settings('uniview-user.sublime-settings')
        commands = settings.get('commands', [])+usettings.get('commands', [])

        self.commands = []
        for command in commands:
            try:
                cmd = command['command']
            except KeyError:
                continue
            try:
                title = command['title']
            except KeyError:
                title = cmd
            self.commands.append([title, cmd])

        sublime.active_window().show_quick_panel(self.commands, self.on_done,  sublime.MONOSPACE_FONT)

    def on_done(self, idx):
        if idx != -1:
            self.view.run_command("uv_command", {"cmd": self.commands[idx][1]})
